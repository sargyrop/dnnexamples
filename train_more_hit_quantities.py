#!/usr/bin/env python3

"""
Simple script to train a neural network with keras
Based on example by Dan Guest: https://github.com/dguest/flow-network
"""

import argparse
from itertools import product
import numpy as np
from matplotlib import pyplot as plt
from math import isclose
import h5py
import os
import json
import sys
import dask.dataframe as dd
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_curve, ConfusionMatrixDisplay, confusion_matrix
from math import sqrt

def get_args():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('input_file', nargs='+')
    parser.add_argument('-e','--epochs', type=int, default=1)
    parser.add_argument('-o','--output-dir', default='model')
    return parser.parse_args()

###############################################################
# Main Training Part
###############################################################

def run():
    args = get_args()

    # get the jets out of the input file (these are np.ndarray)
    jets = None
    hits = None
    list_of_files = []
    for f in args.input_file:
        print(f)
        list_of_files.append(f)
	# Old memory consuming way
        #with h5py.File(f, 'r') as infile:
        #    if jets is None:
        #        jets = np.asarray(infile['jets'])
        #        #hits = np.asarray(infile['hits'])
        #    else:
        #        jets = np.concatenate((jets, np.asarray(infile['jets'])))
        #        #hits = np.concatenate((hits, np.asarray(infile['hits'])))
    
    # Read the files in a dataframe
    df = dd.read_hdf(list_of_files, key='jets')
    print("Dataframe created") 
    
    # Apply cuts
    df = df[ (abs(df['eta']) < 1.6) & (df['pt'] > 20000.) & (((df['pt'] < 60000.) & (df['bTagJVT'] > 0.59)) | (df['pt'] > 60000.)) 
                 & (df['nHits_L0']+df['nHits_L1']+df['nHits_L2']+df['nHits_L3'] > 0)
		 & (df['pt'] > 500000.) & (df['pt'] < 2000000.) & (abs(df['eta']) < 1.6) ]
		 
    # Get a numpy array from the dataframe
    print("Creating array after cuts") 
    jets = df.astype(float).compute().to_records()
    print("Array created")
                
    # only keep jets with high pT and central eta
    make_hit_plots_raw(jets)
            
    bjets = jets[jets['HadronConeExclTruthLabelID']==5]
    ujets = jets[jets['HadronConeExclTruthLabelID']==0]
               
    #print(jets.dtype)
    low_pt_jets = jets[(jets['pt'] < 500000.)  &  (abs(jets['eta']) < 1.6)]
    high_pt_jets = jets[(jets['pt'] > 500000.) & (jets['pt'] < 2000000.) & (abs(jets['eta']) < 1.6)]
    for x in ['nHits_L0', 'nHits_L1', 'nHits_L2', 'nHits_L3']:
        print(f"average {x}    = {np.average(jets[x])}")
        print(f"hits incl b    = {np.average(jets[x][jets['HadronConeExclTruthLabelID']==5])} +- {np.std(jets[x][jets['HadronConeExclTruthLabelID']==5])}")
        print(f"hits incl l    = {np.average(jets[x][jets['HadronConeExclTruthLabelID']==0])} +- {np.std(jets[x][jets['HadronConeExclTruthLabelID']==0])}")
        print(f"hits low pt b  = {np.average(low_pt_jets[x][low_pt_jets['HadronConeExclTruthLabelID']==5])} +- {np.std(low_pt_jets[x][low_pt_jets['HadronConeExclTruthLabelID']==5])}")
        print(f"hits low pt l  = {np.average(low_pt_jets[x][low_pt_jets['HadronConeExclTruthLabelID']==0])} +- {np.std(low_pt_jets[x][low_pt_jets['HadronConeExclTruthLabelID']==0])}")
        print(f"hits high pt b = {np.average(high_pt_jets[x][high_pt_jets['HadronConeExclTruthLabelID']==5])} +- {np.std(high_pt_jets[x][high_pt_jets['HadronConeExclTruthLabelID']==5])}")
        print(f"hits high pt l = {np.average(high_pt_jets[x][high_pt_jets['HadronConeExclTruthLabelID']==0])} +- {np.std(high_pt_jets[x][high_pt_jets['HadronConeExclTruthLabelID']==0])}")

    make_hit_plots(high_pt_jets)

    # pt reweighting
    make_jet_plots(jets)    
    reweighted_b_ind, reweighted_u_ind = DownSampling(high_pt_jets[high_pt_jets['HadronConeExclTruthLabelID']==5], 
    						      high_pt_jets[high_pt_jets['HadronConeExclTruthLabelID']==0])
    
          
    reweighted_b_jets = high_pt_jets[high_pt_jets['HadronConeExclTruthLabelID']==5][reweighted_b_ind]
    reweighted_u_jets = high_pt_jets[high_pt_jets['HadronConeExclTruthLabelID']==0][reweighted_u_ind]
         
    print(f"Reweighted b-labels = {reweighted_b_jets['HadronConeExclTruthLabelID']}")
    print(f"Reweighted u-labels = {reweighted_u_jets['HadronConeExclTruthLabelID']}")
       
    # Make sure the reweighting worked
    plt.clf()
    #plt.hist(0.001*jets['pt_btagJes'][jets['HadronConeExclTruthLabelID']==5], bins=np.arange(0, 5100, 100), alpha=0.3, label='b', density=True)
    #plt.hist(0.001*jets['pt_btagJes'][jets['HadronConeExclTruthLabelID']==0], bins=np.arange(0, 5100, 100), alpha=0.3, label='l', density=True)
    plt.hist(0.001*reweighted_b_jets['pt_btagJes'], bins=np.arange(0, 5050, 50), alpha=0.4, label='b rew', density=True)
    plt.hist(0.001*reweighted_u_jets['pt_btagJes'], bins=np.arange(0, 5050, 50), alpha=0.4, label='l rew', density=True)
    plt.legend()
    plt.ylabel('N jets')
    plt.xlabel(r'$p_T$ rew. [GeV]')
    plt.savefig("jet_pt_reweighted.png")    
            
    # Replace the jets with the reweighted jets
    print(f"N bjets = {len(reweighted_b_jets)} , N l jets = {len(reweighted_u_jets)}")
    high_pt_jets = np.concatenate((reweighted_b_jets, reweighted_u_jets), axis=None)
    print(f"After concatenation {high_pt_jets['pt']} , len = {len(high_pt_jets['pt'])}")
           
    # first, let's make the training dataset!
    mask_value = 999
    jet_inputs = preproc_inputs(high_pt_jets, hits, mask_value)
    #targets = make_targets(jets)
    # type(jet_inputs) and type(jets) is a np.ndarray
    # jet_inputs.dtype is [('pt_btagJes', '<f4'), ...  and jet_inputs.dtype is int32    
    
    print("Initial: ", jet_inputs)
    print("b-jets: " , jet_inputs[jet_inputs[:,8] == 5])
        
    # Keep equal amounts of b and non-b-jets
    b_indices = np.where(jet_inputs[:,8] == 5)[0]
    l_indices = np.where(jet_inputs[:,8] == 0)[0] 
    print("indices of b-jets : ", b_indices)
    print("b-jets from b_indices: ",jet_inputs[b_indices])
    ran_b_indices = np.random.choice(b_indices, size=len(b_indices), replace=False)
    ran_l_indices = np.random.choice(l_indices, size=len(b_indices), replace=False)

    # Exchange the 5's with 1's and the non 5's with 0
    jet_inputs[:,8]=(jet_inputs[:,8] == 5)
    print("b-jets after replacement : ", jet_inputs[b_indices])
    
    print(f"len b = {len(ran_b_indices)} , len non-b = {len(ran_l_indices)}")   
    print(f"Initial jet_inputs size = {len(jet_inputs)}, b-jets size: {len(b_indices)}")
    jet_inputs = np.concatenate((jet_inputs[ran_b_indices], jet_inputs[ran_l_indices]))
    print(f"Final jet_inputs size = {len(jet_inputs)}")
    
    # Shuffle the b-jets and non-b-jets
    np.random.shuffle(jet_inputs) 
    print("shuffled jet_inputs : ", jet_inputs)
    
    plot_dnn_inputs(jet_inputs)
    
    # Split the dataset into inputs and target
    jet_inputs_x = jet_inputs[:,:8] # keep 8 first columns
    jet_inputs_y = jet_inputs[:,8]  # keep 9th column
    # Convert labels to integers
    jet_inputs_y.astype(int)
    print("jet_inputs_x : ", jet_inputs_x)
    print("jet_inputs_y : ", jet_inputs_y)
    
    # Make sure that the inputs do not have nan or inf anywhere
    isnan = sum(np.isnan(jet_inputs_x).any(axis=1))
    isinf = sum(np.isnan(jet_inputs_x).any(axis=1))
    
    if isnan+isinf > 0:
        print("ERROR: inputs contain nan or inf")
        sys.exit(1)
    else:
        print("OK no nan or inf in inputs")	
            
    # split the dataset into training and validation
    rand = np.random.rand(len(jet_inputs_x)) 
    size_train = 0.7
    size_val = 0.15
    size_test = 0.15
    if (not isclose(size_train+size_val+size_test, 1)):
        print(f"ERROR: the size of the samples does not add up to 1: {size_train+size_val+size_test}")
        sys.exit(1)
    training_data_x, val_data_x, training_data_y, val_data_y = train_test_split(jet_inputs_x, jet_inputs_y, test_size=1.0-size_train)	
    val_data_x, testing_data_x, val_data_y, testing_data_y = train_test_split(val_data_x, val_data_y, test_size=size_test/(size_val+size_test))    
    
    print(f"Size training: {len(training_data_x)} , Size validation: {len(val_data_x)} , Size testing: {len(testing_data_x)}")
    print(f"N b-jets used in training: {len(jet_inputs_y[jet_inputs_y==1])}")
    print(f"N l-jets used in training: {len(jet_inputs_y[jet_inputs_y!=1])}")
                            
    # now make the network (for the simple model we don't need all this stuff but I leave it here
    # so that we can expand)
    from keras.layers import Input, TimeDistributed, Dense, Softmax, Masking
    from keras.layers import Concatenate, Dropout
    from keras import backend as kbe
    from keras.models import Model, Sequential
    from keras.callbacks import ReduceLROnPlateau
    import tensorflow as tf
    from keras import regularizers
    
    # Simple model
    model = Sequential()
    model.add(Dense(200, activation='relu', input_dim=8, 
    			kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=1e-4),
    			bias_regularizer=regularizers.l2(1e-4),
    			activity_regularizer=regularizers.l2(1e-5)))
    #model.add(Dropout(0.1))
    model.add(Dense(200, activation='relu', 
    			kernel_regularizer=regularizers.l1_l2(l1=1e-5, l2=1e-4),
    			bias_regularizer=regularizers.l2(1e-4),
    			activity_regularizer=regularizers.l2(1e-5)))
    #model.add(Dropout(0.1))
    model.add(Dense(1, activation='sigmoid'))
    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=1e-4),
                  loss=tf.keras.losses.BinaryCrossentropy(), 
                  metrics=['accuracy'])
    callback_reduce_lr=ReduceLROnPlateau(monitor='val_loss', factor=0.2, 
                                         min_lr=1e-6, patience=3, verbose=1) 

    # Call model predict - we should get 50% accuracy and we do so I comment it out 
#    mean = 0
#    num = 50
#    for _ in range(num):
#        model = Sequential()
#        model.add(Dense(20, input_dim=8, activation='relu'))
#        model.add(Dense(20, activation='relu'))
#        model.add(Dense(1, activation='sigmoid'))
#        model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=1e-5),
#                      loss=tf.keras.losses.BinaryCrossentropy(),
#                      metrics=['accuracy'])
#        output = model.predict(training_data_x)
#        #print("Prediction", output)
#        minPred = np.amin(output)
#        maxPred = np.amax(output)
#        threshold = 0.5*(maxPred+minPred)
#        #print(f"max pred = {maxPred}, min pred = {minPred}, threshold = {threshold}")
#        #print(f"Prediction > threshold: = {len(output[output > threshold])} , size = {len(output)}")
#        mean += len(output[output > threshold]) / len(output)
#        del model
#    mean /= num
#    print(f"outputs > mean: {100*mean} %")
#    sys.exit(0)
          
    # now fit this thing!
    history = model.fit(training_data_x, training_data_y, batch_size=64, epochs=args.epochs, validation_data=(val_data_x, val_data_y))
    
    # make some control plots
    plot(history)
    
    # Make NN control plots and ROC curves
    makeNNplots(model,testing_data_x,testing_data_y)
    
    # finally, save the trained network
    odir = args.output_dir
    if not os.path.isdir(odir):
        os.mkdir(odir)
    with open(f'{odir}/architecture.json','w') as arch_file:
        arch_file.write(model.to_json(indent=2))
    model.save_weights(f'{odir}/weights.h5')

    # also write out the variable specification
    with open(f'{odir}/variables.json', 'w') as vars_file:
        json.dump(get_variables_json(), vars_file)


def make_targets(jets):
    labels = jets['HadronConeExclTruthLabelID']
    targets = np.stack([labels == x for x in [0, 4, 5]], axis=1)
    return np.asarray(targets, dtype=int)


def get_discrim(jets, model_path, weights_path):
    """
    Get the discriminant from a saved model
    """

    # this just silences some annoying warnings that I get from
    # tensorflow. They might be useful for training but in evaluation
    # they should be harmless.
    os.environ['TF_CPP_MIN_LOG_LEVEL']='2'

    # load the model
    from keras.models import model_from_json
    with open(model_path,'r') as model_file:
        model = model_from_json(model_file.read())
    model.load_weights(weights_path)

    # get the model inputs
    input_data = preproc_inputs(jets)
    outputs = model.predict(input_data)
    b_weight, light_weights = outputs[:,2], outputs[:,0]
    output_log_ratio = np.log(b_weight / light_weights)
    return output_log_ratio



####################################################################
# Preprocessing and saving the configuration
####################################################################
#

def hit_vars():
    pos = [f'{x}_local' for x in ['x','y','z']]
    return pos

def jet_vars():
    ip = [f'IP{x}D_p{y}' for x, y in product([2,3],'bcu')]
    jf = [
        f'JetFitter_{x}' for x in ['significance3d', 'mass','energyFraction']]
    hits = [ f'nHits_L{x}' for x in ['0','1','2','3']]
    dR = [ f'hitdR_L{x}' for x in ['0','1','2','3']]
    labels = ['HadronConeExclTruthLabelID']
    #return ip + jf
    return hits + dR + labels

def preproc_inputs(jets, hits, mask_value):
    """
    We make some hardcoded transformations to normalize these inputs
    """
        
    normalise=True # if True normalise else standardise
      
    # Jets
    jet_array = np.stack([jets[x] for x in jet_vars()], axis=1).astype(float)

    print("jet_array before = ", jet_array)
    
    # Remove any rows with nan or inf values
    jet_array = jet_array[~np.isnan(jet_array).any(axis=1) & ~np.isinf(jet_array).any(axis=1)]    
        
    # For the dR entries, we don't want to take into account values with -1 (=0 hits) in the standardisation
    jet_array_temp = np.copy(jet_array)
    for i in range(4,8):
      jet_array_temp[:,i][jet_array_temp[:,i] < 0] = np.nan
              
    for i in range(0,8):      
        print(f"mean column {i} = {np.nanmean(jet_array_temp[:,i])} - stdev = {np.nanstd(jet_array_temp[:,i])} - min = {np.nanmin(jet_array_temp[:,i])} - max = {np.nanmax(jet_array_temp[:,i])}")	
        if normalise: jet_array[:,i]=(jet_array[:,i]-np.nanmin(jet_array_temp[:,i]))/(np.nanmax(jet_array_temp[:,i])-np.nanmin(jet_array_temp[:,i]))
        else: jet_array[:,i]=(jet_array[:,i]-np.nanmean(jet_array[:,i]))/np.nanstd(jet_array_temp[:,i])
        #jet_array[:,i] = np.nan_to_num(jet_array[:,i])
    print("jet_array after = ", jet_array)
    
    return jet_array


def get_variables_json():
    """
    Make a file that specifies the input variables and
    transformations as JSON, so that the network can be used with lwtnn
    """

    # This is a more 'traditional' network with one set of inputs so
    # we just have to name the variables in one input node. In more
    # advanced cases we could have multiple input nodes, some of which
    # might operate on sequences.
    btag_variables = [
        {
            # Note this is not the same name we use in the file! We'll
            # have to make the log1p transformation in the C++ code to
            # build this variable.
            'name': 'jf_sig_log1p',
            # 'offset': JF_OFFSET,
            # 'scale': JF_SCALE,
        },
        {
            'name': 'rnnip_log_ratio',
            # 'offset': RNN_OFFSET,
            # 'scale': RNN_SCALE,
            # 'default': RNN_DEFAULT,
        }
    ]

    # note that this is a list of output nodes, where each node can
    # have multiple output values. In principal we could also add a
    # regression output to the same network (i.e. the b-hadron pt) but
    # that's a more advanced subject.
    outputs = [
        {
            'name': 'classes',
            'labels': ['light', 'charm', 'bottom']
        }
    ]

    # lwtnn expects a specific format that allows multiple input and
    # output nodes, so we have to dress the above information a bit.
    final_dict = {
        'input_sequences': [],
        'inputs': [
            {
                'name': 'btag_variables',
                'variables': btag_variables
            }
        ],
        'outputs': outputs
    }

    return final_dict

def plot(history):
    plt.clf()
    plt.plot(history.history['accuracy'])
    plt.plot(history.history['val_accuracy'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'val'], loc='upper left')
    plt.savefig("model_accuracy.png")
    
    plt.clf()
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'val'], loc='upper left')
    plt.savefig("model_loss.png")    

def plotER(b, u, bins, label, plotName):
    eff = np.zeros(bins.shape)
    rej = np.zeros(bins.shape)
    S   = np.zeros(bins.shape)
    
    for i, val in enumerate(bins):
        eff[i] = len(b[b > val])/len(b)
        rej[i] = len(u[u < val])/len(u)
        S[i]   = len(b[b > val]) / sqrt(len(u[u > val])) if  len(u[u > val]) > 0 else 0
    
    plt.clf()
    plt.plot(eff, label='b eff')
    plt.plot(rej, label='l rej')
    plt.plot(S, label='S', linestyle='--')
    plt.legend(loc='upper left')
    plt.ylabel('Efficiency and rejection')
    plt.xlabel(f"{label}")
    plt.savefig(f"{plotName}.png")

def makeNNplots(model,testing_data_x,testing_data_y):
    # NN score plot
    plt.clf()
    y_pred = model.predict(testing_data_x)
    plt.hist(y_pred, bins=np.arange(0, 1.05, 0.05))
    plt.ylabel('N jets')
    plt.xlabel('NN score')
    plt.savefig("NNscore.png")
    
    # NN score for b-jets and non-b-jets separately
    plt.clf()
    plt.hist(y_pred[testing_data_y==1], bins=np.arange(0, 1.05, 0.05), label='b-jets', alpha=0.5, density=True)
    plt.hist(y_pred[testing_data_y==0], bins=np.arange(0, 1.05, 0.05), label='non b-jets', alpha=0.5, density=True)
    plt.legend(loc='upper left')
    plt.ylabel(r'$1/N_{\mathrm{jets}}\cdot dN_{\mathrm{jets}}/d\mathrm{NN}$')
    plt.xlabel('NN score')
    plt.savefig("NNscore_bl_norm.png")
    
    # ROC curve
    plt.clf()
    ns_probs = [0 for _ in range(len(testing_data_y))]
    ns_fpr, ns_tpr, _ = roc_curve(testing_data_y, ns_probs)
    fpr, tpr, thresholds = roc_curve(testing_data_y, y_pred)
    plt.plot(ns_fpr, ns_tpr, linestyle='--', label='Luck')
    plt.plot(fpr, tpr, marker='.', label='DNN')
    plt.ylabel('True Positive Rate')
    plt.xlabel('False Positive Rate')
    roc_auc = np.trapz(tpr, x=fpr)
    plt.text(0.0,1.0, f"AUC = {roc_auc:4.3f}")
    plt.savefig("ROC.png")
    
    # Confusion matrix
    plt.clf()
    y_pred = y_pred > 0.5
    labels = ["b-jet", "not b-jet"]
    cm = confusion_matrix(testing_data_y, y_pred)
    disp = ConfusionMatrixDisplay(confusion_matrix=cm, display_labels=labels)
    disp.plot(cmap=plt.cm.Blues)
    plt.savefig("CM.png")

def make_hit_plots(jets):
    plt.clf()
    plt.hist(0.001*jets['pt'][(jets['HadronConeExclTruthLabelID'] == 5)], bins=np.arange(0, 1005, 5), alpha=0.6, label='b-jets', density=True)
    plt.hist(0.001*jets['pt'][jets['HadronConeExclTruthLabelID'] == 0], bins=np.arange(0, 1005, 5), alpha=0.4, label='l-jets', density=True)
    plt.hist(0.001*jets['pt'][jets['HadronConeExclTruthLabelID'] == 4], bins=np.arange(0, 1005, 5), alpha=0.4, label='c-jets', density=True)
    plt.hist(0.001*jets['pt'][jets['HadronConeExclTruthLabelID'] == 15], bins=np.arange(0, 1005, 5), alpha=0.4, label=r'$\tau$-jets', density=True)
    plt.legend()
    plt.ylabel('N jets')
    plt.xlabel(r'$p_{T}$ [GeV]')
    plt.savefig("jet_pt_perflavour.png")
    
    plt.clf()
    plt.scatter(0.001*jets['pt'][(jets['HadronConeExclTruthLabelID'] == 5)], jets['nHits_L0'][(jets['HadronConeExclTruthLabelID'] == 5)])
    plt.xlabel(r'$p_{T}$ [GeV]')
    plt.ylabel('N hits L0')
    plt.savefig("b_jet_pt_vs_nHits_L0.png")
    
    plt.clf()
    plt.scatter(0.001*jets['pt'][(jets['HadronConeExclTruthLabelID'] == 0)], jets['nHits_L0'][(jets['HadronConeExclTruthLabelID'] == 0)])
    plt.xlabel(r'$p_{T}$ [GeV]')
    plt.ylabel('N hits L0')
    plt.savefig("l_jet_pt_vs_nHits_L0.png")
    
    plt.clf()
    plt.hist(jets['hitdR_L0'], bins=np.arange(0, 0.042, 0.002), alpha=0.6,  label='L0', density=True)
    plt.hist(jets['hitdR_L1'], bins=np.arange(0, 0.042, 0.002), alpha=0.6 , label='L1', density=True)
    plt.hist(jets['hitdR_L2'], bins=np.arange(0, 0.042, 0.002), alpha=0.6 , label='L2', density=True)
    plt.hist(jets['hitdR_L2'], bins=np.arange(0, 0.042, 0.002), alpha=0.6 , label='L3', density=True)
    plt.legend()
    plt.ylabel('N jets')
    plt.xlabel(r'$\langle\Delta R(\mathrm{hit,jet})\rangle$')
    plt.savefig("averageDRHitJet.png")
 
    for x in ['0', '1', '2', '3']: 
        plt.clf()
        plt.hist(jets[f'nHits_L{x}'][jets['pt'] < 300000], bins=np.arange(0, 102, 2), alpha=0.6, label=r'$p_{T} < 300$ GeV', density=True)
        plt.hist(jets[f'nHits_L{x}'][(jets['pt'] > 300000) & (jets['pt'] < 600000)], bins=np.arange(0, 102, 2), alpha=0.6 , label=r'$300 < p_{T} < 600$ GeV', density=True)
        plt.hist(jets[f'nHits_L{x}'][jets['pt'] > 600000], bins=np.arange(0, 102, 2), alpha=0.6 , label=r'$p_{T} > 600$ GeV', density=True)
        plt.legend()
        plt.ylabel('N jets')
        plt.xlabel(f'N hits L{x}')
        plt.savefig(f"nHits_L{x}_differentPt.png")
	
        plt.clf()
        plt.hist(jets[f'nHits_L{x}'][(jets['HadronConeExclTruthLabelID'] == 5)], bins=np.arange(0, 42, 2), alpha=0.5,  label=f'L{x} - b-jets', density=True)
        plt.hist(jets[f'nHits_L{x}'][(jets['HadronConeExclTruthLabelID'] == 0)], bins=np.arange(0, 42, 2), alpha=0.5 , label=f'L{x} - l-jets', density=True)
        plt.legend()
        plt.ylabel('N jets')
        plt.xlabel(f'N hits L{x}')
        plt.savefig(f"nHits_L{x}_selected_BvsL.png")
	
        plt.clf()
        plt.hist(jets[f'hitdR_L{x}'][(jets['HadronConeExclTruthLabelID'] == 5)], bins=np.arange(0, 0.042, 0.002), alpha=0.5,  label=f'L{x} - b-jets', density=True)
        plt.hist(jets[f'hitdR_L{x}'][(jets['HadronConeExclTruthLabelID'] == 0)], bins=np.arange(0, 0.042, 0.002), alpha=0.5 , label=f'L{x} - l-jets', density=True)
        plt.legend()
        plt.ylabel('N jets')
        plt.xlabel(r'$\langle\Delta R(\mathrm{hit,jet})\rangle$')
        plt.savefig(f"averageDRHitJet_perFlav_L{x}.png")

	
def plot_dnn_inputs(jets):
    for x in range(4): 
        plt.clf()
        b=jets[:,x][jets[:,8]==1].astype(float)
        l=jets[:,x][jets[:,8]==0].astype(float)
        print("plot b", b)
        print("plot l", l)
        (unique, counts) = np.unique(b, return_counts=True)
        frequencies = np.asarray((unique, counts)).T
        print("frequencies", frequencies)
        plt.hist(b, bins=np.arange(0, 1.01, 0.01), alpha=0.5, label='b', density=True)
        plt.hist(l, bins=np.arange(0, 1.01, 0.01), alpha=0.5, label='l', density=True)
        plt.legend()
        plt.ylabel('N jets')
        plt.xlabel('N hits (standardised)')
        plt.savefig(f"nHits_L{x}_stand_dNN_inputs.png")
    
    for x in range(4,8): 
        plt.clf()
        b=jets[:,x][jets[:,8]==1].astype(float)
        l=jets[:,x][jets[:,8]==0].astype(float)
        print("plot b", b)
        print("plot l", l)
        (unique, counts) = np.unique(b, return_counts=True)
        frequencies = np.asarray((unique, counts)).T
        print("frequencies", frequencies)
        plt.hist(b, bins=np.arange(0, 1.01, 0.01), alpha=0.5, label='b', density=True)
        plt.hist(l, bins=np.arange(0, 1.01, 0.01), alpha=0.5, label='l', density=True)
        plt.legend()
        plt.ylabel('N jets')
        plt.xlabel('dR(hit,,jet) (standardised)')
        plt.savefig(f"dRhit_L{x-4}_stand_dNN_inputs.png")


def multJump(a1, a2):
    return np.divide(a2-a1, a2, out=-np.ones(a2.shape)*(-99), where=a2!=0)

def make_hit_plots_raw(jets):
    for x in range(4): 
        plt.clf()
        plt.hist(jets[f'nHits_L{x}'][jets['HadronConeExclTruthLabelID']==5], bins=np.arange(0, 61, 1), alpha=0.5, label='b', density=True)
        plt.hist(jets[f'nHits_L{x}'][jets['HadronConeExclTruthLabelID']==0], bins=np.arange(0, 61, 1), alpha=0.5, label='l', density=True)
        plt.legend()
        plt.ylabel('N jets')
        plt.xlabel('N hits')
        plt.savefig(f"nHits_L{x}_raw.png")
	
        plt.clf()
        plt.hist(jets[f'hitdR_L{x}'][jets['HadronConeExclTruthLabelID']==5], bins=np.arange(0, 0.041, 0.001), alpha=0.5, label='b', density=True)
        plt.hist(jets[f'hitdR_L{x}'][jets['HadronConeExclTruthLabelID']==0], bins=np.arange(0, 0.041, 0.001), alpha=0.5, label='l', density=True)
        plt.legend()
        plt.ylabel('N jets')
        plt.xlabel('dR(hit,jet)')
        plt.savefig(f"dRhit_L{x}_raw.png")
    
    A=jets['hitdR_L0'][jets['HadronConeExclTruthLabelID']==5]
    B=jets['hitdR_L0'][jets['HadronConeExclTruthLabelID']==0]
    print(f"dR_L0 eff = {len(A[(A>0.008) & (A<0.021)])/len(A)} , eff_bg = {len(B[(B>0.008) & (B<0.021)])/len(B)}")
      
    sumHits=jets['nHits_L0']+jets['nHits_L1']+jets['nHits_L2']+jets['nHits_L3']
    plt.clf()
    plt.hist(sumHits[jets['HadronConeExclTruthLabelID']==5], bins=np.arange(0, 101, 1), alpha=0.5, label='b', density=True)
    plt.hist(sumHits[jets['HadronConeExclTruthLabelID']==0], bins=np.arange(0, 101, 1), alpha=0.5, label='l', density=True)
    plt.legend()
    plt.ylabel('N jets')
    plt.xlabel('N hits L0+L1+L2+L3')
    plt.savefig("nHits_sumLayers_raw.png")
    
    A=sumHits[jets['HadronConeExclTruthLabelID']==5]
    B=sumHits[jets['HadronConeExclTruthLabelID']==0]
    print(f"sumHits eff = {len(A[(A>8) & (A<32)])/len(A)} , eff_bg = {len(B[(B>8) & (B<32)])/len(B)}")
    
    plotER(sumHits[jets['HadronConeExclTruthLabelID']==5], sumHits[jets['HadronConeExclTruthLabelID']==0], bins=np.arange(0, 101, 1), 
           label="N hits L0+L1+L2+L3", plotName="ER_sumHits")
    
    j01=multJump(jets['nHits_L0'], jets['nHits_L1'])
    j12=multJump(jets['nHits_L1'], jets['nHits_L2'])
    j23=multJump(jets['nHits_L2'], jets['nHits_L3'])
    j02=multJump(jets['nHits_L0'], jets['nHits_L2'])
    j03=multJump(jets['nHits_L0'], jets['nHits_L3'])
    j13=multJump(jets['nHits_L1'], jets['nHits_L3'])
    
    soap=j01+j12+j23+j02+j03+j13
    maxp=np.maximum.reduce([j01,j12,j23,j02,j03,j13])
    moap=np.mean([j01,j12,j23,j02,j03,j13],axis=0)
    
    A=soap[jets['HadronConeExclTruthLabelID']==5]
    B=soap[jets['HadronConeExclTruthLabelID']==0]
    print(f"soap eff = {len(A[A>2])/len(A)} , eff_bg = {len(B[B>2])/len(B)}")
    
    A=j03[jets['HadronConeExclTruthLabelID']==5]
    B=j03[jets['HadronConeExclTruthLabelID']==0]
    print(f"j03 eff = {len(A[A>0.6])/len(A)} , eff_bg = {len(B[B>0.6])/len(B)}")
    
    plt.clf()
    plt.hist(j01[jets['HadronConeExclTruthLabelID']==5], bins=np.arange(-2., 5.2, 0.2), alpha=0.5, label='b', density=True)
    plt.hist(j01[jets['HadronConeExclTruthLabelID']==0], bins=np.arange(-2., 5.2, 0.2), alpha=0.5, label='l', density=True)
    plt.legend()
    plt.ylabel('N jets')
    plt.xlabel('N1-N0/N1')
    plt.savefig("jump_01.png")
    
    plt.clf()
    plt.hist(j03[jets['HadronConeExclTruthLabelID']==5], bins=np.arange(-2., 5.2, 0.2), alpha=0.5, label='b', density=True)
    plt.hist(j03[jets['HadronConeExclTruthLabelID']==0], bins=np.arange(-2., 5.2, 0.2), alpha=0.5, label='l', density=True)
    plt.legend()
    plt.ylabel('N jets')
    plt.xlabel('N3-N0/N3')
    plt.savefig("jump_03.png")
    
    plt.clf()
    plt.hist(soap[jets['HadronConeExclTruthLabelID']==5], bins=np.arange(-2., 5.2, 0.2), alpha=0.5, label='b', density=True)
    plt.hist(soap[jets['HadronConeExclTruthLabelID']==0], bins=np.arange(-2., 5.2, 0.2), alpha=0.5, label='l', density=True)
    plt.legend()
    plt.ylabel('N jets')
    plt.xlabel('Sum of all jumps')
    plt.savefig("soap.png")
    
    plt.clf()
    plt.hist(maxp[jets['HadronConeExclTruthLabelID']==5], bins=np.arange(-2., 5.2, 0.2), alpha=0.5, label='b', density=True)
    plt.hist(maxp[jets['HadronConeExclTruthLabelID']==0], bins=np.arange(-2., 5.2, 0.2), alpha=0.5, label='l', density=True)
    plt.legend()
    plt.ylabel('N jets')
    plt.xlabel('Max of all jumps')
    plt.savefig("maxjump.png")
    
    plt.clf()
    plt.hist(moap[jets['HadronConeExclTruthLabelID']==5], bins=np.arange(-2., 5.2, 0.2), alpha=0.5, label='b', density=True)
    plt.hist(moap[jets['HadronConeExclTruthLabelID']==0], bins=np.arange(-2., 5.2, 0.2), alpha=0.5, label='l', density=True)
    plt.legend()
    plt.ylabel('N jets')
    plt.xlabel('Mean of all jumps')
    plt.savefig("moap.png")
    
    
def make_jet_plots(jets):
    plt.clf()
    b, bins, _ = plt.hist(0.001*jets['pt'][jets['HadronConeExclTruthLabelID']==5], bins=np.arange(0, 3100, 100), alpha=0.5, label='b', density=True)
    l, bins, _ = plt.hist(0.001*jets['pt'][jets['HadronConeExclTruthLabelID']==0], bins=np.arange(0, 3100, 100), alpha=0.5, label='l', density=True)
    #plt.plot(reweighted_x, 0.5*(bins[1:]+bins[:-1]), label='reweighted b')
    plt.legend()
    plt.ylabel('N jets')
    plt.xlabel(r'$p_{T}$ [GeV]')
    plt.savefig("jet_pt.png")
    
    plt.clf()
    plt.hist(jets['eta'][jets['HadronConeExclTruthLabelID']==5], bins=np.arange(-2, 2.2, 0.2), alpha=0.5, label='b', density=True)
    plt.hist(jets['eta'][jets['HadronConeExclTruthLabelID']==0], bins=np.arange(-2, 2.2, 0.2), alpha=0.5, label='l', density=True)
    plt.legend()
    plt.ylabel('N jets')
    plt.xlabel(r'$|\eta|$')
    plt.savefig("jet_eta.png")
    

def DownSampling(bjets, ujets):
    # pT in MeV
    pt_bins = np.linspace(0, 7000000, 71)
    
    eta_bins = np.linspace(0, 2.5, 10)

    histvals_b, _, _ = np.histogram2d(bjets['absEta_btagJes'], bjets['pt_btagJes'],
                                [eta_bins, pt_bins])
    histvals_u, _, _ = np.histogram2d(ujets['absEta_btagJes'], ujets['pt_btagJes'],
                                [eta_bins, pt_bins])

    b_locations_pt = np.digitize(bjets['pt_btagJes'], pt_bins) - 1
    b_locations_eta = np.digitize(bjets['absEta_btagJes'], eta_bins) - 1
    b_locations = zip(b_locations_pt, b_locations_eta)
    b_locations = list(b_locations)

    u_locations_pt = np.digitize(ujets['pt_btagJes'], pt_bins) - 1
    u_locations_eta = np.digitize(ujets['absEta_btagJes'], eta_bins) - 1
    u_locations = zip(u_locations_pt, u_locations_eta)
    u_locations = list(u_locations)

    b_loc_indices = { (pti, etai) : [] for pti,_ in enumerate(pt_bins[::-1]) for etai,_ in enumerate(eta_bins[::-1])}
    u_loc_indices = { (pti, etai) : [] for pti,_ in enumerate(pt_bins[::-1]) for etai,_ in enumerate(eta_bins[::-1])}

    for i, x in enumerate(b_locations):
        b_loc_indices[x].append(i)

    for i, x in enumerate(u_locations):
        u_loc_indices[x].append(i)

    bjet_indices = []
    ujet_indices = []

    for pt_bin_i in range(len(pt_bins) - 1):
        for eta_bin_i in range(len(eta_bins) - 1):
            loc = (pt_bin_i, eta_bin_i)

            nbjets = int(histvals_b[eta_bin_i][pt_bin_i])
            nujets = int(histvals_u[eta_bin_i][pt_bin_i])

            njets = min([nbjets, nujets])
            b_indices_for_bin = b_loc_indices[loc][0:njets]
            u_indices_for_bin = u_loc_indices[loc][0:njets]
            bjet_indices += b_indices_for_bin
            ujet_indices += u_indices_for_bin

    bjet_indices.sort()
    ujet_indices.sort()
    return np.array(bjet_indices), np.array(ujet_indices)
    
         
if __name__ == '__main__':
    run()
